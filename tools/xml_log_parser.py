#!/usr/bin/env python3
import urllib.request as urllib
import pickle
import os, sys
from xml.etree import ElementTree as et
import nilsimsa
from datetime import date, timedelta
import gzip
import threading
import time



base_directory = 'data/'


files_ = os.listdir(base_directory)
files = list()
for i in files_:
    if i.endswith(".bin"):
        files.append(i)

for i in files:
    #print(i)
    temp_file = open(base_directory + i, 'rb')
    temp_data = pickle.load(temp_file)
    for h in temp_data:
        line = str(h.decode("utf-8")).replace("\n", "")
        try:
            root = et.fromstring(line)
            if root.attrib["Routine"] == "startupPass1":
                print (line)
        except:
            pass
